const faker = require('faker')
const Moment = require('moment')

let data = []
const today = Moment().utc().format()
const createRange = Moment().subtract(2, 'years').utc().format()
const updateRange = Moment().subtract(1, 'year').utc().format()

for(let i = 0; i < 100; i++) {
    let lastName = faker.name.lastName()

    data.push({
        id: data.length + 1,
        firstName: faker.name.firstName(),
        lastName: lastName,
        role_id: 1,
        username: faker.internet.userName(lastName),
        password: faker.internet.password(),
        grade: faker.datatype.number({
            min: 1,
            max: 12
        }),
        createdAt: faker.date.between(createRange, updateRange),
        updatedAt: faker.date.between(updateRange, today)
    })
}

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('Students', data)
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('Students', null, {})
  }
};
