const {
  sequelize
} = require('../models')
const {
  QueryTypes
} = require('sequelize')
const faker = require('faker')
const Moment = require('moment')

module.exports = {
  up: async (queryInterface, Sequelize) => {
    let data = []
    const today = Moment().utc().format()
    const createRange = Moment().subtract(2, 'years').utc().format()
    const updateRange = Moment().subtract(1, 'year').utc().format()

    const query = `SELECT stu.id AS student_id, stu.grade, su.id AS subject_id, cha.id AS chapter_id FROM Students stu
    JOIN Subjects su ON su.grade = stu.grade
    JOIN Chapters cha ON cha.subject_id = su.id`

    try {
      await sequelize.query(query, {
        type: QueryTypes.SELECT
      }).then(result => {
        let num = 0

        for (let i = 0; i < result.length; i++) {

          data.push({
            id: num += 1,
            ...result[i],
            score: faker.datatype.number({
              min: 10,
              max: 100
            }),
            createdAt: faker.date.between(createRange, updateRange),
            updatedAt: faker.date.between(updateRange, today)
          })
        }
      })
    } catch (error) {
      console.log(error)
    }

    await queryInterface.bulkInsert('Reports', data)
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('Reports', null, {})
  }
};