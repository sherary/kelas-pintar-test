let data = []
const subjects = ['Math', 'Indonesian', 'English', 'Science']
const faker = require('faker')
const Moment = require('moment')

const today = Moment().utc().format()
const createRange = Moment().subtract(2, 'years').utc().format()
const updateRange = Moment().subtract(1, 'year').utc().format()

for(let i = 0; i < subjects.length; i++) {
    for(let j = 1; j <= 12; j++) {
        data.push({
            id: data.length + 1,
            grade: j,
            subject_name: subjects[i],
            createdAt: faker.date.between(createRange, updateRange),
            updatedAt: faker.date.between(updateRange, today)
        })
    }
}

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('Subjects', data)
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('Subjects', null, {})
  }
};
