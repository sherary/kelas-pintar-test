const faker = require('faker')
const Moment = require('moment')

const today = Moment().utc().format()
const createRange = Moment().subtract(2, 'years').utc().format()
const updateRange = Moment().subtract(1, 'year').utc().format()

//Math
const Q1_Math = ['Addition', 'Subtraction', 'Multiplication', 'Division']
const Q2_Math = ['The Nature of Numbers', 'Fraction & Decimals', 'Pre-algebra', 'Linear Equations']
const Q3_Math = ['Algebra', 'Geometry', 'Trigonometry', 'Statistics & Probability']

let data = []

for(let i = 1; i <= 4; i++) {
    for(let j = 0; j < Q1_Math.length; j++) {
        data.push({
            id: data.length + 1,
            subject_id: i,
            chapter_name: Q1_Math[j],
            createdAt: faker.date.between(createRange, updateRange),
            updatedAt: faker.date.between(updateRange, today)
        })
    }
}

for(let k = 5; k <= 8; k++) {
    for(let l = 0; l < Q2_Math.length; l++) {
        data.push({
            id: data.length + 1,
            subject_id: k,
            chapter_name: Q2_Math[l],
            createdAt: faker.date.between(createRange, updateRange),
            updatedAt: faker.date.between(updateRange, today)
        })
    }
}

for(let m = 9; m <= 12; m++) {
    for(let n = 0; n < Q3_Math.length; n++) {
        data.push({
            id: data.length + 1,
            subject_id: m,
            chapter_name: Q3_Math[n],
            createdAt: faker.date.between(createRange, updateRange),
            updatedAt: faker.date.between(updateRange, today)
        })
    }
}

//Indonesian
const Q1_Indonesian = ['Arah Mata Angin & Membaca Peta', 'Menulis Halus', 'Cerita Rakyat', 'Mengarang Indah']
const Q2_Indonesian = ['Peribahasa & Prosa', 'Majas', 'Penyusunan Naskah Pidato', 'Penggunaan EYD']
const Q3_Indonesian = ['Sastra Indonesia', 'Menyusun Naskah Drama', 'Teknik Surat Menyurat', 'Menyusun Ide Artikel']

for(let a = 13; a <= 16; a++) {
    for(let b = 0; b < Q1_Indonesian.length; b++) {
        data.push({
            id: data.length + 1,
            subject_id: a,
            chapter_name: Q1_Indonesian[b],
            createdAt: faker.date.between(createRange, updateRange),
            updatedAt: faker.date.between(updateRange, today)
        })
    }
}

for(let c = 17; c <= 20; c++) {
    for(let d = 0; d < Q2_Indonesian.length; d++) {
        data.push({
            id: data.length + 1,
            subject_id: c,
            chapter_name: Q2_Indonesian[d],
            createdAt: faker.date.between(createRange, updateRange),
            updatedAt: faker.date.between(updateRange, today)
        })
    }
}

for(let e = 21; e <= 24; e++) {
    for(let f = 0; f < Q3_Indonesian.length; f++) {
        data.push({
            id: data.length + 1,
            subject_id: e,
            chapter_name: Q3_Indonesian[f],
            createdAt: faker.date.between(createRange, updateRange),
            updatedAt: faker.date.between(updateRange, today)
        })
    }
}

//English
const Q1_English = ['Sustained Reading', 'Cursive Writing', 'Syntax & Vocabulary', 'Thematic Writing']
const Q2_English = ['Phonics Fluency', 'Reading Comprehension', 'Composition & Grammar Building', 'English Literature']
const Q3_English = ['Speech Practice', 'Language Study', 'Research & Composition', 'Classic Literature']

for(let g = 25; g <= 28; g++) {
    for(let h = 0; h < Q1_English.length; h++) {
        data.push({
            id: data.length + 1,
            subject_id: g,
            chapter_name: Q1_English[h],
            createdAt: faker.date.between(createRange, updateRange),
            updatedAt: faker.date.between(updateRange, today)
        })
    }
}

for(let i = 29; i <= 32; i++) {
    for(let j = 0; j < Q2_English.length; j++) {
        data.push({
            id: data.length + 1,
            subject_id: i,
            chapter_name: Q2_English[j],
            createdAt: faker.date.between(createRange, updateRange),
            updatedAt: faker.date.between(updateRange, today)
        })
    }
}

for(let k = 33; k <= 36; k++) {
    for(let l = 0; l < Q3_English.length; l++) {
        data.push({
            id: data.length + 1,
            subject_id: k,
            chapter_name: Q3_English[l],
            createdAt: faker.date.between(createRange, updateRange),
            updatedAt: faker.date.between(updateRange, today)
        })
    }
}

//Science
const Q1_Science = ['Human Body', 'Plants & Animals', 'Seasons & Weathers', 'Planets']
const Q2_Science = ['Structure, Function & Information Processing', 'Growth, Development & Reproductions of Organisms', 'Matter & Energy', 'Natural Ecosystem']
const Q3_Science = ['Physics', 'Chemistry', 'Biology', 'Forensics']

for(let m = 37; m <= 40; m++) {
    for(let n = 0; n < Q1_Science.length; n++) {
        data.push({
            id: data.length + 1,
            subject_id: m,
            chapter_name: Q1_Science[n],
            createdAt: faker.date.between(createRange, updateRange),
            updatedAt: faker.date.between(updateRange, today)
        })
    }
}

for(let o = 41; o <= 44; o++) {
    for(let p = 0; p < Q2_Science.length; p++) {
        data.push({
            id: data.length + 1,
            subject_id: o,
            chapter_name: Q2_Science[p],
            createdAt: faker.date.between(createRange, updateRange),
            updatedAt: faker.date.between(updateRange, today)
        })
    }
}

for(let q = 45; q <= 48; q++) {
    for(let r = 0; r < Q3_Science.length; r++) {
        data.push({
            id: data.length + 1,
            subject_id: q,
            chapter_name: Q3_Science[r],
            createdAt: faker.date.between(createRange, updateRange),
            updatedAt: faker.date.between(updateRange, today)
        })
    }
}

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('Chapters', data)
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('Chapters', null, {})
  }
};
