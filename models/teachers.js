'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class Teachers extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here

        }
    };
    Teachers.init({
        firstname: {
            type: DataTypes.STRING,
            allowNull: false
        },

        lastname: {
            type: DataTypes.STRING,
            allowNull: false
        },

        role_id: {
            type: DataTypes.BIGINT,
            allowNull: false,
            defaultValue: 2
        },

        username: {
            type: DataTypes.STRING,
            allowNull: false
        },

        password: {
            type: DataTypes.STRING,
            allowNull: false
        },

        subject_id: {
            type: DataTypes.BIGINT,
            allowNull: false,
            references: {
                model: 'Subjects',
                key: 'id'
            }
        }
    }, {
        sequelize,
        modelName: 'Teachers',
        tableName: 'Teachers',
        freezeTableName: true
    });

    return Teachers;
};