'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Reports extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  Reports.init({
    student_id: {
      type: DataTypes.BIGINT,
      references: {
        model: 'Students',
        key: 'id'
      }
    },

    grade: {
      type: DataTypes.INTEGER
    },

    subject_id: {
      type: DataTypes.BIGINT,
      references: {
        model: 'Subjects',
        key: 'id'
      }
    },

    chapter_id: {
      type: DataTypes.BIGINT,
      references: {
        model: 'Chapters',
        key: 'id'
      }
    },

    score: {
      type: DataTypes.INTEGER,
      defaultValue: 0
    }
  }, {
    sequelize,
    modelName: 'Reports',
    tableName: 'Reports',
    freezeTableName: true
  });

  return Reports;
};