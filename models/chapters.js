'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Chapters extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  Chapters.init({
    
    subject_id: {
      type: DataTypes.BIGINT,
      references: {
        model: 'Subjects',
        key: 'id'
      }
    },

    chapter_name: {
      type: DataTypes.STRING,
      allowNull: false
    }
  }, {
    sequelize,
    modelName: 'Chapters',
    tableName: 'Chapters',
    freezeTableName: true
  });

  return Chapters;
};