const express = require('express');
const app = express();
const db = require('./models');
require('dotenv').config();

const PORT = process.env.PORT || 3000;

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

const userRoutes = require('./routes/userRoutes');
const reportRoutes = require('./routes/reportRoutes')
app.use('/users', userRoutes);
app.use('/reports', reportRoutes);

db.sequelize.sync().then(() => {
    console.log(`Database connected!`)
    app.listen(PORT, () => {
        console.log(`🚀Server run on ${process.env.HOST}:${PORT}🚀`)
    })
}).catch(error => {
    console.log(error)
});

module.exports = app;