const Students = class {
    async register (req, res) {
        const { firstname, lastname, username, password } = req.body

        return res.status(200).json({
            code: res.statusCode,
            message: 'Register success!',
            data: {
                username: username,
                firstname: firstname,
                lastname: lastname
            }
        })
    }

    async login (req, res) {
        const { username, password } = req.body

        return res.status(200).json({
            code: res.statusCode,
            message: 'Login success',
            data: {
                user: req.body
            }
        })
    }
}

module.exports = new Students;