const { Students, Teachers, Subjects, Chapters, Reports, sequelize, Sequelize } = require('../models')

const StudentsReports = class {

    constructor() {
        Students.hasMany(Reports, {
            foreignKey: 'student_id'
        })
        Subjects.hasMany(Teachers, {
            foreignKey: 'subject_id'
        })
        Subjects.hasMany(Reports, {
            foreignKey: 'subject_id'
        })
        Subjects.hasMany(Chapters, {
            foreignKey: 'subject_id'
        })
        Chapters.hasOne(Reports, {
            foreignKey: 'chapter_id'
        })

        Chapters.belongsTo(Subjects, {
            foreignKey: 'subject_id'
        })
        Reports.belongsTo(Subjects, {
            foreignKey: 'subject_id'
        })
        Teachers.belongsTo(Subjects, {
            foreignKey: 'subject_id'
        })
        Reports.belongsTo(Students, {
            foreignKey: 'student_id'
        })
        Reports.belongsTo(Chapters, {
            foreignKey: 'chapter_id'
        })

    }

    async byStudentID (req, res) {
        try {
            const { student_id } = req.params

            let data = await Reports.findAll({
                where: {
                    student_id: student_id
                },
                raw: true,
                include: [{
                    model: Students,
                    attributes: [
                        [
                            Sequelize.fn('CONCAT',
                            Sequelize.col('firstname'), ' ', 
                            Sequelize.col('lastname')),
                            'student_name'
                        ]
                    ],
                }, {
                    model: Subjects,
                    attributes: [['subject_name', 'name']]
                }, {
                    model: Chapters,
                    attributes: [['chapter_name', 'name']]
                }],
            })

            return res.status(200).json({
                code: res.statusCode,
                message: 'Success',
                data: data
            })
        } catch (err) {
            console.log(err)
            return res.status(404).json({
                code: res.statusCode,
                message: 'Data not found'
            })
        }
    }

    async getAll (req, res) {
        try {
            let data = await Reports.findAll({ 
                raw: true,
                include: [{
                    model: Students,
                    attributes: [
                        [
                            Sequelize.fn('CONCAT',
                            Sequelize.col('firstname'), ' ', 
                            Sequelize.col('lastname')),
                            'student_name'
                        ]
                    ],
                }, {
                    model: Subjects,
                    attributes: [['subject_name', 'name']]
                }, {
                    model: Chapters,
                    attributes: [['chapter_name', 'name']]
                }],
            })

            return res.status(200).json({
                code: res.statusCode,
                message: 'Success get all reports',
                data: data
            })
        } catch (err) {
            console.log(err)
            return res.status(500).json({
                code: res.statusCode,
                message: 'Something went wrong'
            })
        }
    }
}

module.exports = new StudentsReports;