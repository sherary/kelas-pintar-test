const { Students, Teachers } = require('../models')
const bcrypt = require('bcrypt');
require('dotenv').config();

const UserController = class {
    async register (req, res) {
        try {
            if (req.body.role_id == 1) {
                Students.create(req.body)

                res.status(200).json({
                    code: res.statusCode,
                    message: 'Register success!',
                    data: {
                        name: req.body.firstname + ' ' + req.body.lastname,
                        username: req.body.username,
                        grade: req.body.grade,
                    },
                    token: req.body.token
                })
            } else if (req.body.role_id == 2) {
                Teachers.create(req.body)

                res.status(200).json({
                    code: res.statusCode,
                    message: 'Register success!',
                    data: {
                        name: req.body.firstname + ' ' + req.body.lastname,
                        username: req.body.username,
                    },
                    token: req.body.token
                })
            }
        } catch (err) {
            return res.status(500).json({ message: 'Register error' })
        }
    }

    async login (req, res) {
        let data = []
        try {
            if (req.user.role_id == 1) {
                const student = await Students.findOne({
                    where: {
                        username: req.user.username
                    },
                    raw: true
                })

                const match = bcrypt.compareSync(req.user.password, student.password);
                if (match == true && req.body.username == req.user.username) {
                    data.push(student)
                } else {
                    return res.status(403).json({ message: 'Wrong username or password'})
                }
            } else if (req.user.role_id == 2) {
                const teacher = await Teachers.findOne({
                    where: {
                        username: req.user.username
                    },
                    raw: true
                })

                const match = bcrpy.compareSync(req.user.password, teacher.password);
                if (match == true && req.body.username == req.user.username) {
                    data.push(teacher)
                } else {
                    return res.status(403).json({ message: 'Wrong username or password'})
                }
            }
            return res.status(200).json({
                code: res.statusCode,
                message: 'Login success!',
                data: data
            })
        } catch (err) {
            return res.status(500).json({ message: 'Login error' })
        }
    }
}

module.exports = new UserController;