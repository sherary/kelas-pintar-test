'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Teachers', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.BIGINT
      },

      firstname: {
        type: Sequelize.STRING,
        allowNull: false
      },

      lastname: {
        type: Sequelize.STRING,
        allowNull: false
      },

      role_id: {
        type: Sequelize.BIGINT,
        allowNull: false,
        default: 2
      },

      username: {
        type: Sequelize.STRING,
        allowNull: false
      },

      password: {
        type: Sequelize.STRING,
        allowNull: false
      },

      subject_id: {
        type: Sequelize.BIGINT,
        allowNull: false,
        reference: {
          model: 'Subjects',
          key: 'id'
        }
      },

      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },

      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Teachers');
  }
};