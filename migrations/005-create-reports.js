'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Reports', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.BIGINT
      },

      student_id: {
        type: Sequelize.BIGINT,
        references: {
          model: 'Students',
          key: 'id'
        }
      },

      grade: {
        type: Sequelize.INTEGER
      },

      subject_id: {
        type: Sequelize.BIGINT,
        references: {
          model: 'Subjects',
          key: 'id'
        }
      },

      chapter_id: {
        type: Sequelize.BIGINT,
        references: {
          model: 'Chapters',
          key: 'id'
        }
      },

      score: {
        type: Sequelize.INTEGER,
        default: 0
      },

      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },

      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Reports');
  }
};