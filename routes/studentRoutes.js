const express = require('express');
const router = express.Router();
const passport = require('passport');
const auth = require('../middlewares/auth');
const userValidator = require('../middlewares/userValidators');
const StudentController = require('../controllers/studentControllers');

router.post('/register', [
    userValidator.register,
    passport.authenticate('register', {
        session: false
    })
], StudentController.register)

router.post('/login', [
    userValidator.login,
    passport.authenticate('login', {
        session: false
    })
], StudentController.login)

module.exports = router;