const express = require('express');
const router = express.Router();
const ReportController = require('../controllers/reportControllers');

router.get('/:student_id', ReportController.byStudentID)
router.get('/', ReportController.getAll);

module.exports = router;