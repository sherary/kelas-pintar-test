const express = require('express');
const router = express.Router();
const { Students } = require('../models/students')
const userValidator = require('../middlewares/userValidators');
const auth = require('../middlewares/auth');
const UserController = require('../controllers/userControllers');

router.post('/register', userValidator.registerStudent, auth.register, UserController.register);
router.post('/login', userValidator.login, auth.login, UserController.login);

module.exports = router;