const Joi = require('joi');
const { Students } = require('../models');

const schemas = {
    firstname: Joi.string().trim().min(2).max(20).required()
        .error(errors => {
            errors.forEach(err => {
                if (err.code == 'any.required' || err.code == 'any.empty') {
                    err.message = 'Firstname cannot be empty'
                } else if (err.code == 'string.min' || err.code == 'string.max') {
                    err.message = 'Please enter valid surname'
                } else {
                    console.error(err.code);
                }
            });
            return errors;
        }),

    lastname: Joi.string().trim().min(2).max(20).required()
        .error(errors => {
            errors.forEach(err => {
                if (err.code == 'any.required' || err.code == 'any.empty') {
                    err.message = 'Lastname cannot be empty'
                } else if (err.code == 'string.min' || err.code == 'string.max') {
                    err.message = 'Please enter valid lastname'
                } else {
                    console.error(err.code);
                }
            });
            return errors;
        }),

    grade: Joi.number().min(1).max(12).required()
        .error(errors => {
            errors.forEach(err => {
                if (err.code == 'any.required' || err.code == 'any.empty') {
                    err.message = 'Please provide your grade'
                } else if (err.code == 'number.min' || err.code == 'number.max') {
                    err.message = 'Grade must be between 1 to 12'
                } else {
                    console.error(err.code);
                }
            })
            return errors
        }),
    
    role_id: Joi.number().min(1).max(2).required()
        .error(errors => {
            errors.forEach(err => {
                if (err.code == 'number.min' || err.code == 'number.max') {
                    err.message = 'Invalid role id'
                } else {
                    console.error(err.code);
                }
            })
            return errors
        }),

    username: Joi.string().trim().min(8).max(32).required()
        .error(errors => {
            errors.forEach(err => {
                if (err.code == 'any.required' || err.code == 'any.empty') {
                    err.message = 'Username cannot be empty'
                } else if (err.code == 'string.min' || err.code == 'string.max') {
                    err.message = 'Username must be at least 8 characters'
                } else {
                    console.error(err.code)
                }
            });
            return errors;
        }),

    password: Joi.string().min(8).max(32).required()
        .error(errors => {
            errors.forEach(err => {
                if (err.code == 'any.required' || err.code == 'any.empty') {
                    err.message = 'Password cannot be empty'
                } else if (err.code == 'string.min' || err.code == 'string.max') {
                    err.message = 'Password must be at least 8 characters!'
                } else {
                    console.error(err.code);
                }
            })
            return errors;
        }),

    password_confirmation: Joi.ref('password'),
}

exports.registerStudent = async (req, res, next) => {
    try {
        const schema = Joi.object().keys({
            firstname: schemas.firstname,
            lastname: schemas.lastname,
            grade: schemas.grade,
            role_id: schemas.role_id,
            username: schemas.username,
            password: schemas.password,
            password_confirmation: schemas.password_confirmation
        }).with('password', 'password_confirmation')
        
        await schema.validateAsync(req.body)

        next();
    } catch (error) {
        return res.status(422).json({
            code: res.statusCode,
            errors: error.message
        })
    }
}

exports.login = async (req, res, next) => {
    try {
        const schema = Joi.object().keys({
            username: schemas.username,
            password: schemas.password
        })

        await schema.validateAsync(req.body)

        next();
    } catch (error) {
        return res.status(422).json({
            code: res.statusCode,
            message: error.message
        })
    }
}