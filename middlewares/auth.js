const jwt = require('jsonwebtoken');
require('dotenv').config();
const { Students, Teachers } = require('../models');
const bcrypt = require('bcrypt');

module.exports.register = async (req, res, next) => {
    if (req.body.role_id == 1) {
        let student = await Students.findOne({
            where: {
                username: req.body.username
            }, 
            raw: true
        })

        if(student) return res.status(422).json({ message: 'This username is already taken!'})
    } else if (req.body.role_id == 2) {
        let teacher = await Teachers.findOne({
            where: {
                username: req.body.username
            },
            raw: true
        })

        if(teacher) return res.status(422).json({ message: 'This username is already taken!'})
    } else {
        return res.status(403).json({ message: 'Not a valid user'})
    }

    delete req.body.password_confirmation

    req.body.password = bcrypt.hashSync(req.body.password, 10)
    
    const token = jwt.sign(req.body, process.env.SECRET_TOKEN, {
        expiresIn: '7d'
    })
    req.body.token = token

    next()
}

module.exports.login = async (req, res, next) => {
    const header = req.headers['authorization']
    if(header == undefined) return res.status(401).json({ message: 'No token = no access'})
    
    const token = header.split(' ') [1]
    jwt.verify(token, process.env.SECRET_TOKEN, (err, user) => {
        if (err) return res.status(403).json({ message: 'Bad Request'})
        req.user = user
        next()
    })
}
